---
layout: markdown_page
title: "Gartner Market Guide for DevOps Value Stream Delivery Platforms"
description: "This page represents how Gartner views the Value Stream Delivery Platform market in 2020."
canonical_path: "/analysts/gartner-vsdp20/"
---
### Gartner Market Guide for DevOps Value Stream Delivery Platforms

Since our inception, GitLab has focused on delivering an end-to-end DevOps platform as a single application. That's why we were so excited to read the [2020 Market Guide for DevOps Value Stream Platforms](https://page.gitlab.com/resources-report-gartner-market-guide-vsdp/) (VSDPs), in which Gartner has defined an emerging category of DevOps Tools.

**Gartner's Market Definition:**

"DevOps value stream delivery platforms provide a fully integrated set of capabilities to enable continuous delivery of software. These capabilities may include project or product planning, build automation, continuous integration, test automation, continuous deployment and rollback, release orchestration, and automated security policy enforcement, and may provide visibility to key value stream metrics.
 
VSDPs make product and platform teams more efficient by alleviating constraints in the software delivery pipeline. This results in improved flow of work from initial customer contact to production support. VSDPs are extensible by design and expose APIs for other tools to manage and monitor the software delivery life cycle. The goal is to improve the flow of value rather than automate a sequence of disparate tasks."



### The Market Perspective

**GitLab believes** that end-to-end visibility is essential to shift from a project mindset to a product mindset and create and adapt a steady stream of end-user value. That visibility creates opportunities to learn and improve while proactively identifying and remediating value blockers, prioritize across multiple resources and teams, and allow each stakeholder--regardless of role--to understand context and contribute more effectively.

**Gartner states:**
  * “By using a common platform across development, security and operations activities, DevOps teams can bridge gaps and foster collaboration between organizational silos.”
  * “Using a unified platform provides end-to-end visibility, thus reducing the time to value and improving efficiency and consistency. This end-to-end view encourages systems thinking over local optimization, and enables organizations to continuously improve through accelerated feedback loops.”

_______

**GitLab believes** that businesses should focus on delivering the greatest amount of value, rather than maintaining the infrastructure necessary to understand, produce, or deploy it.

**Gartner states:**
  * “Using IaC capabilities in VSDPs not only enables infrastructure agility, but also visibility into infrastructure spend (which is especially critical for public cloud deployments).”
  * “Scale DevOps initiatives by adopting value stream delivery platforms (VSDPs) that reduce the overhead of managing complex toolchains and meet the needs of multiple product teams.”

_______

**GitLab believes** that security and compliance are everyone’s responsibility, and should be as deeply integrated into the SDLC as planning or testing.

**Gartner states:** “The need to “shift left” security and compliance is a result of a multitude of factors and risks associated with arbitrary use of open-source software and shorter release cycles.”

Given Gartner’s Strategic Planning Assumption (_“By 2023, 40% of organizations will have switched from multiple point solutions to DevOps value stream delivery platforms to streamline application delivery, versus less than 10% in 2020.”_), GitLab believes that Gartner aligns with our belief that the market for a DevOps platform delivered as a single application will grow much faster than the DevOps market as a whole.

_______
### How we're different:

While we're honored to be included as a representative vendor in Gartner's Market Guide, and we look forward to many years of developing coverage, we also have a very opinionated take on what a DevOps platform should offer, which creates a very different product. 

Key differentiators include:

* A platform that extends beyond the scope of the current VSDP definition, providing all the tools you need to move from ideation to delivery to learning.
* GitLab is infrastructure-agnostic--run in the cloud of your choice, on-premises, or through GitLab.com, our SaaS service.
* GitLab’s product does more than bundle together common developer tools. We’ve found that we can dramatically improve developer experience and efficiency by integrating what was a disparate tool-chain. Our single application, single data store and single permission model allows developers to focus less on maintaining integrations, and more on shipping value to their customers.

We have proof points in this enhanced efficiency and experience in our first to market combination of source-control and continuous integration. By freeing developers to manage their CI configuration as code, and empowering them to run and improve their inner build-test loop we unlocked a major hurdle to DevOps adoption. We continue to do the same with integrated Application Security, Project, Package, Release, Infrastructure and Incident Management. Users of our single-application get the benefits of more control, and more insights - unlocking the metrics that drive DevOps success.

_______
_Gartner, Market Guide for DevOps Value Stream Delivery Platforms, Manjunath Bhat, Hassan Ennaciri, Chris Saunderson, Daniel Betts, Thomas Murphy, Joachim Herschmann, 28 September 2020_

_Gartner does not endorse any vendor, product or service depicted in its research publications, and does not advise technology users to select only those vendors with the highest ratings or other designation. Gartner research publications consist of the opinions of Gartner's research organization and should not be construed as statements of fact. Gartner disclaims all warranties, expressed or implied, with respect to this research, including any warranties of merchantability or fitness for a particular purpose._
