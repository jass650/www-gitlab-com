# Adding a new application to `/applications`

Edit [`data/applications.yml`](../data/applications.yml) and add a new entry within
the correct categories `applications` list.

Please  add a `.png` image to [`source/images/applications/apps`](../source/images/applications/apps),
the name of the image should be the same as the title, but with underscores instead of spaces.

Example:

```yaml
...
  - title: My new application
    content: My new application description.
    links:
      - url: https://my-new-application.io
        title: my-new-application.io
      - ...
...
```

- The image should be located in `source/images/applications/apps/my_new_application.png`.
- The application `content` string will be truncated to 100 characters. Please do not include any HTML tags.
- The application `links` list will be truncated to 3 links.

## Adding a new category

If you **need** to create a new category, you can do so.

Please  add an `.svg` image to [`source/images/applications/categories`](../source/images/applications/categories),
the name of the image should be the same as the category id, but with underscores instead of hyphens.

Example:

```yaml
...
- title: My new category
  id: my-new-category
  applications:
    - ...
    - ...
...
```

The image should be located in `source/images/applications/categories/my_new_category.svg`.
